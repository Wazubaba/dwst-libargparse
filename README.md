WST - D branch: libargparse
===========================

This module is meant to act as a sort of helper for dealing
with unix-style commandline parsing. The overall goal was a
relatively simple API and being able to work around it in
the event of special needs, such as my [DImCha project](https://gitlab.com/Wazubaba/dimcha-dport)'s
`-x` arg.

All functions have inline documentation, buildable via
`dub build -b docs`.

# 30-mile-high overview
First, you need to initialize an Opts struct. After this,
you call Opts.register() for each arg you wish to listen
for.
	
For the flag and subArgs arguments, provide a reference
pointer (`&`) to a valid variable, and that will contain the
data, or throw NotEnoughSubArgs if an invalid number of args
were provided on the command line for a given flag.

For an in-action use of the library, see [DImCha's main.d](https://gitlab.com/Wazubaba/dimcha-dport/blob/master/src/main.d)

