DC= dmd
DFLAGS=

OUTNAME= argparse

.PHONY: release debug test clean all package

release: i386/lib$(OUTNAME).a x86_64/lib$(OUTNAME).a include/dwst/$(OUTNAME).di
release32: i386/lib$(OUTNAME).a include/dwst/$(OUTNAME).di
release64: x86_64/lib$(OUTNAME).a include/dwst/$(OUTNAME).di

debug: i386/lib$(OUTNAME)-debug.a x86_64/lib$(OUTNAME)-debug.a include/dwst/$(OUTNAME).di
debug32: i386/lib$(OUTNAME)-debug.a include/dwst/$(OUTNAME).di
debug64: x86_64/lib$(OUTNAME)-debug.a include/dwst/$(OUTNAME).di

test: i386/lib$(OUTNAME)-test x86_64/lib$(OUTNAME)-test
test32: i386/lib$(OUTNAME)-test
test64: x86_64/lib$(OUTNAME)-test

docs: docs/$(OUTNAME).html

package: release/lib$(OUTNAME)-release.tar.gz release/lib$(OUTNAME)-debug.tar.gz release/lib$(OUTNAME)-source.tar.gz release/lib$(OUTNAME)-release.zip release/lib$(OUTNAME)-debug.zip release/lib$(OUTNAME)-source.zip
all: release debug test docs


#### COMPILING RULES ####

#####################
# 32bit Compile rules
#####################

### Object builders ###
i386/obj-release/$(OUTNAME).o: src/$(OUTNAME).d
	$(DC) $(DFLAGS) -m32 -c -release -O -of$@ $<

i386/obj-debug/$(OUTNAME).o: src/$(OUTNAME).d
	$(DC) $(DFLAGS) -m32 -c -g -debug -of$@ $<

i386/obj-test/$(OUTNAME).o: src/$(OUTNAME).d
	$(DC) $(DFLAGS) -m32 -c -g -debug -unittest -main -of$@ $<

### Lib builders ###
i386/lib$(OUTNAME).a: i386/obj-release/$(OUTNAME).o
	@mkdir -p i386
	$(DC) $(DFLAGS) -m32 -lib -release -O -of$@ $<

i386/lib$(OUTNAME)-debug.a: i386/obj-debug/$(OUTNAME).o
	@mkdir -p i386
	$(DC) $(DFLAGS) -m32 -lib -g -debug -of$@ $<

### Unittest builder ###
i386/lib$(OUTNAME)-test: i386/obj-test/$(OUTNAME).o
	@mkdir -p i386
	$(DC) $(DFLAGS) -m32 -g -debug -unittest -of$@ $<

#####################
# 64bit Compile rules
#####################

### Object Builders ###
x86_64/obj-release/$(OUTNAME).o: src/$(OUTNAME).d
	$(DC) $(DFLAGS) -m64 -c -release -O -of$@ $<

x86_64/obj-debug/$(OUTNAME).o: src/$(OUTNAME).d
	$(DC) $(DFLAGS) -m64 -c -g -debug -of$@ $<

x86_64/obj-test/$(OUTNAME).o: src/$(OUTNAME).d
	$(DC) $(DFLAGS) -m64 -c -g -debug -unittest -of$@ $<

### Lib builders ###
x86_64/lib$(OUTNAME).a: x86_64/obj-release/$(OUTNAME).o
	@mkdir -p x86_64
	$(DC) $(DFLAGS) -m64 -lib -release -O -of$@ $<

x86_64/lib$(OUTNAME)-debug.a: x86_64/obj-debug/$(OUTNAME).o
	@mkdir -p x86_64
	$(DC) $(DFLAGS) -m64 -lib -g -debug -of$@ $<

### Unittest builder ###
x86_64/lib$(OUTNAME)-test: x86_64/obj-test/$(OUTNAME).o
	@mkdir -p x86_64
	$(DC) $(DFLAGS) -m64 -g -debug -unittest -main -of$@ $<

###############################################################################

### Header file generation ###
include/dwst/$(OUTNAME).di:
	@mkdir -p include/dwst
	$(DC) $(DFLAGS) -o- -H -Hf$@ src/$(OUTNAME).d

### Documentation generation ###
docs/$(OUTNAME).html:
	@mkdir docs
	$(DC) $(DFLAGS) -o- -D -Df$@ src/$(OUTNAME).d

###############################
# PACKAGE CONSTRUCTION RULES
###############################
.lib$(OUTNAME)-release-package: release docs
	@mkdir -p $@
	@cp -r i386 $@/.
	@cp -r x86_64 $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

.lib$(OUTNAME)-debug-package: debug docs
	@mkdir -p $@
	@cp -r i386 $@/.
	@cp -r x86_64 $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

.lib$(OUTNAME)-source-package: docs include/dwst/$(OUTNAME).di
	@mkdir -p $@
	@cp -r src $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

release/lib$(OUTNAME)-release.tar.gz: .lib$(OUTNAME)-release-package
	@mkdir -p release
	@tar c $< > release/lib$(OUTNAME)-release.tar
	@gzip release/lib$(OUTNAME)-release.tar

release/lib$(OUTNAME)-debug.tar.gz: .lib$(OUTNAME)-debug-package
	@mkdir -p release
	@tar c $< > release/lib$(OUTNAME)-debug.tar
	@gzip release/lib$(OUTNAME)-debug.tar

release/lib$(OUTNAME)-source.tar.gz: .lib$(OUTNAME)-source-package
	@mkdir -p release
	@tar c $< > release/lib$(OUTNAME)-source.tar
	@gzip release/lib$(OUTNAME)-source.tar

release/lib$(OUTNAME)-release.zip: .lib$(OUTNAME)-release-package
	@mkdir -p release
	@zip $@ -r $<

release/lib$(OUTNAME)-debug.zip: .lib$(OUTNAME)-debug-package
	@mkdir -p release
	@zip $@ -r $<

release/lib$(OUTNAME)-source.zip: .lib$(OUTNAME)-source-package
	@mkdir -p release
	@zip -r $@ $<

###############################################################################

#### UTILITY RULES ####
clean:
	-@$(RM) -r i386 x86_64
	-@$(RM) -r include docs
	-@$(RM) -r release .lib$(OUTNAME)-release-package .lib$(OUTNAME)-debug-package .lib$(OUTNAME)-source-package

install: release docs
	@mkdir -p $(DESTDIR)/usr/lib/i386-linux-gnu/.
	@cp i386/lib$(OUTNAME).a $(DESTDIR)/usr/lib/i386-linux-gnu/.

	@mkdir -p $(DESTDIR)/usr/lib/x86_64-linux-gnu/.
	@cp x86_64/lib$(OUTNAME).a $(DESTDIR)/usr/lib/x86_64-linux-gnu/.

	@mkdir -p $(DESTDIR)/usr/include/dwst
	@cp include/dwst$(OUTNAME).di $(DESTDIR)/usr/include/dwst/.

	@mkdir -p $(DESTDIR)/usr/share/doc/dwst/libwtar
	@cp docs/$(OUTNAME).html $(DESTDIR)/usr/share/doc/dwst/lib$(OUTNAME)/.

uninstall:
	$(RM) $(DESTDIR)/usr/lib/i386-linux-gnu/lib$(OUTNAME).a
	$(RM) $(DESTDIR)/usr/lib/x86_64-linux-gnu/lib$(OUTNAME).a
	$(RM) $(DESTDIR)/usr/include/dwst/$(OUTNAME).di
	$(RM) -r $(DESTDIR)/usr/share/doc/dwst/lib$(OUTNAME)
